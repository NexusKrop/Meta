# NexusKrop

This is **NexusKrop**, a collection of projects that varied from game modifications to general software. This repository is currently under construction.

## Projects

Our primary projects:

- [IceCube](https://gitea.com/NexusKrop/IceCube)
- [IceShell](https://gitea.com/NexusKrop/IceShell)

## Community

- General organisation issues: [Here](https://gitea.com/NexusKrop/Meta/issues)
- IRC Channel: `#nexuskrop` at `irc.rizon.net`
